import React, { useState } from 'react';

export default function Profile() {

    let listTweets = [
        {
            id: 1,
            title: "titre1",
            username: "username1",
            content: "contenu1"
        },
        {
            id: 2,
            title: "titre2",
            username: "username2",
            content: "contenu2"
        },
        {
            id: 3,
            title: "titre3",
            username: "username3",
            content: "contenu3"
        }
    ];

    const [tweets, setTweet] = useState(listTweets);
    
    function deleteTweet(tweet) {
        setTweet(tweets.filter((tweetToDelete) => tweetToDelete.id !== tweet.id)); 
    };

    return (
      <div>
        <div>
            {
                tweets.map(tweet => 
                <div key={tweet.id} >
                    <h5>{tweet.title}</h5>
                    <span>{tweet.username}</span>
                    <p>{tweet.content}</p>
                    <button onClick={() => deleteTweet(tweet)}>Supprimer</button>
                </div>)
            }
        </div>
      </div>
    )
  }
