import React, { useState } from 'react';

export default function AddTweet() {

    const [tweets, setTweet] = useState([]);
    let title = "";
    let content = ""; 
    let author = ""; 

    function addTweet(e: Event) {
      console.log(e);
    };

    function handleChangeAuthor(e) {
      console.log(e.target.value);
    };

    return (
      <form onSubmit={addTweet}>
        <label>
          Titre :
          <input type="text" name="name" value={title}/>
        </label>
        <label>
          Contenu du Tweet :
          <input type="text" name="name" value={content}/>
        </label>
        <label>
          Author :
          <input type="text" name="name" onChange={(e) => handleChangeAuthor(e)}/>
        </label>
        <input type="submit" value="Envoyer" />
      </form>
    )
  }

  export type Tweet = {
    title: string,
    content: string,
    author: string
  }
